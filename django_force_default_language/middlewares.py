from django.conf import settings
from django.http.response import HttpResponseRedirect
from django.utils import translation
from django.urls import translate_url


def get_path_from_request(request) -> str:
    """
    Return current path from request
    """

    return request.get_full_path()


def get_translated_url(lang_code: str, url: str, translate_to: str) -> str:
    translation.activate(lang_code)
    
    return translate_url(url, lang_code=translate_to)


def get_default_lang_code():
    return (
        getattr(settings, 'FORCE_DEFAULT_LANGUAGE_CODE', None) or 
        getattr(settings, 'LANGUAGE_CODE', None)
    )


class LanguageRedirectMiddleware:
    def __init__(self, get_response=None):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        response = self.process_response(request, response)

        return response

    def process_response(self, request, response):
        if (
            request.path.startswith(getattr(settings,'FORCE_DEFAULT_LANGUAGE_EXCLUDE_PATHES', ()))
            or response.status_code == 404
        ):
            return response

        current_language = translation.get_language()
        default_language = get_default_lang_code()
        previous_url = request.session.pop('previous_url', request.META.get('HTTP_REFERER'))
        user_language = request.get_signed_cookie(
            settings.FORCE_DEFAULT_LANGUAGE_COOKIE_NAME, default=None
        )

        # If user came from another site - set default language, not from url
        if user_language is None and current_language != default_language:
            new_language = default_language

            translated_url = get_translated_url(
                user_language, get_path_from_request(request), new_language
            )

            translation.activate(new_language)
            request.LANGUAGE_CODE = new_language
            response = HttpResponseRedirect(redirect_to=translated_url)
        elif user_language and current_language != user_language:
            if previous_url is None:
                new_language = user_language
            else:
                new_language = current_language

            # We have to activate previous language to proper of translate_url
            translated_url = get_translated_url(
                user_language, get_path_from_request(request), new_language
            )
            
            # After that activate new language
            translation.activate(new_language)
            request.LANGUAGE_CODE = new_language

            response = HttpResponseRedirect(redirect_to=translated_url)

            request.session['previous_url'] = request.build_absolute_uri(request.path)
        else:
            new_language = current_language

        response.set_signed_cookie(settings.FORCE_DEFAULT_LANGUAGE_COOKIE_NAME, new_language)

        return response
