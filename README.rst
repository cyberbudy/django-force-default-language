
Django Force Default Language
=============================

Forces default language code for a user, when user came from another website

Installation
^^^^^^^^^^^^

.. code-block:: python

   pip install django_force_default_language

Update settings

.. code-block:: python

   # Coockie name is used to identify default language
   FORCE_DEFAULT_LANGUAGE_COOKIE_NAME = '_USER_LANGUAGE'
   # Forces this language as the first for user, when LANGUAGE_CODE is not suited for you
   FORCE_DEFAULT_LANGUAGE_CODE = 'uk'
   # Exclude some API's from being redirected
   FORCE_DEFAULT_LANGUAGE_EXCLUDE_PATHES = (
       '/admin',
       '/api',
       '/rosetta',
       '/uploads',
       '/sitemap',
       '/robots',
       '/facebook',
       '/google',
       '/social',
   )


   # Add middleware at the end of your middlewares
   MIDDLEWARES += [
       'django_force_default_language.middlewares.LanguageRedirectMiddleware'
   ]
